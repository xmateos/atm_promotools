<?php

namespace ATM\PromotoolsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateXmlCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('atm:promotools:generate:xml');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $kernel = $this->getApplication()->getKernel();
        $container = $kernel->getContainer();
        $em = $container->get('doctrine')->getManager('promotools');
        $config = $container->getParameter('atm_promotools_config');
        $siteName = $config['site_name'];
        $paysitePlaceholder = $config['paysite_placeholder'];
        $hwcdn = $container->get('xlabs_hwcdn');
        $currentDate = new \DateTime();


        $mediaFolder = $container->getParameter('media_folder');
        $rootPath = $kernel->getRootDir().'/../web/media/'.$mediaFolder.'/atm_promotools';
        if(!is_dir($rootPath)){
            mkdir($rootPath);
        }

        $query = "SELECT a.id,
                             a.title,
                             a.description,
                             a.pornstars,
                             a.tags,
                             a.creation_date,
                             s.name as siteName
                      FROM advertisement a 
                      JOIN site s ON s.id = a.site_id
                      WHERE s.name LIKE '%".$siteName."%'
                      ORDER BY creation_date DESC";

        $statement = $em->getConnection()->prepare($query);
        $statement->execute();
        $results = $statement->fetchAll();

        $dom = new \DOMDocument();
        $videosElem = $dom->createElement('videos');
        $videosElem->appendChild($dom->createElement('updated',$currentDate->format('d-m-Y H:i:s')));
        $videosElem->appendChild($dom->createElement('total',count($results)));
        foreach($results as $result){
            $videoQuery = "SELECT v.id,
                                  v.path,
                                  v.duration,
                                  v.canonical,
                                  v.title,
                                  v.description,
                                  v.pornstars,
                                  v.tags,
                                  t.path as thumb_path
                           FROM video v
                           LEFT JOIN thumbnail t on t.video_id = v.id
                           WHERE advertisement_id =".$result['id'];

            $statement = $em->getConnection('promotools')->prepare($videoQuery);
            $statement->execute();
            $video = $statement->fetchAll();
            if(!empty($video)){

                $videoElem = $dom->createElement('video');
                $videoElem->appendChild($dom->createElement('id',$video[0]['id']));
                $videoElem->appendChild($dom->createElement('title',htmlspecialchars($video[0]['title'])));
                $videoElem->appendChild($dom->createElement('description',htmlspecialchars($video[0]['description'])));
                $videoElem->appendChild($dom->createElement('tags',$video[0]['tags']));
                $videoElem->appendChild($dom->createElement('paysite',$paysitePlaceholder));
                $urlVideo = $hwcdn->getCDNResource(array('media_path'=>'/'.$video[0]['path']));
                $clipTextNode = $dom->createCDATASection($urlVideo);
                $clipNode = $dom->createElement('clipurl');
                $clipNode->appendChild($clipTextNode);
                $videoElem->appendChild($clipNode);
                $videoElem->appendChild($dom->createElement('duration',$video[0]['duration']));

                if(isset($video['thumb_path'])){
                    $thumbsElem = $dom->createElement('thumbs');
                    $url = $hwcdn->getCDNResource(array('media_path'=>'/'.$video['thumb_path']));
                    $thumbsElem->appendChild($dom->createElement('thumbnail',$url));
                    $videoElem->appendChild($thumbsElem);
                }

                $videosElem->appendChild($videoElem);
            }
            $dom->appendChild($videosElem);
        }
        $dom->save($rootPath.'/'.$siteName.'.xml');
    }
}