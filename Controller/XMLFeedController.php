<?php

namespace ATM\PromotoolsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class XMLFeedController extends Controller{
    /**
     * @Route("/xml/feed" , name="atm_promotools_xml_feed")
     */
    public function xmlFeedAction(){
        $config = $this->getParameter('atm_promotools_config');
        $siteName = $config['site_name'];

        $request = $this->get('request_stack')->getCurrentRequest();
        $affiliateId = $request->get('affiliate_id');
        $affiliateId = is_null($affiliateId) ? 'affiliate_id':$affiliateId;

        $mediaFolder = $this->getParameter('media_folder');
        $xml = file_get_contents($this->get('kernel')->getRootDir().'/../web/media/'.$mediaFolder.'/atm_promotools/'.$siteName.'.xml');

        $xmlReplaced = str_replace('affiliate_id',$affiliateId,$xml);

        $response = new Response($xmlReplaced);
        $response->headers->set('Content-Type', 'text/xml');

        return $response;

    }
}